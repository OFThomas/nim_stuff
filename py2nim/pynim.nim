import nimpy

proc add*(a,b: float): float {.exportpy.} =
  return a + b
