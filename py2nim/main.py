import nimporter    # necessary 
import pynim        # my nim library

# Then all you need is this line 
nimporter.build_nim_extensions()

# main
if(__name__ == "__main__"):

    print(pynim.add(2, 4))

