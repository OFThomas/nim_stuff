import std/strutils
import std/sequtils
import std/random
import std/rdstdin
import std/times
import std/os

# nice colours
import std/terminal

# read the word list at compile time 
const word_size = 5
const full_word_list: string = static_read("word_list.txt")
const num_words = countlines(full_word_list)
const word_list = split_lines(full_word_list)

# seeds 
if paramCount() > 0: 
  let arg = paramStr(1)
  if arg in "h,-h,--h,help":
    echo "Run wordle to use the current date as the seed"
    echo "Run wordle -r to generate a random word"
    echo "Run wordle 23472 to use the seed `23472` for example"
    quit(0)
  if arg in "r,-r,--r":
    randomize()
    echo "using random seed"
  else:
    let int_arg = parseInt(arg)
    randomize(int_arg) # init random device 
    echo "using ", int_arg, " seed"
else:
  let time = parseInt(now().format("yyyyMMdd"))
  randomize(time)
  echo "using time ", time, " seed"

let choice = rand(num_words)
let word: string = word_list[choice]

type guess_t = enum
    black, grey, green, yellow

proc colour(incol: guess_t): ForegroundColor =
  if incol == green:
    return fgGreen
  elif incol == yellow:
    return fgRed
  elif incol == grey:
    return fgDefault
  else:
    return fgBlack

proc checkguess(guess: string, word: string): seq[guess_t] =
  var valid: seq[guess_t] = newseq[guess_t](word.len)

  var already_checked: string
  # check all valid letters first
  for idx, c in guess:
    if c == word[idx]:
      valid[idx] = green
      already_checked.add(c)

  # after checking all the correct positions do a second pass 
  for idx, c in guess:
    if valid[idx] == green:
      continue
    elif c in word and already_checked.count(c) < word.count(c):
      valid[idx] = yellow
      already_checked.add(c)
    else:
      valid[idx] = grey

  return valid

const alphabet: string = "qwertyuiopasdfghjklzxcvbnm"
var key_state: seq[guess_t] = newseq[guess_t](alphabet.len)
for i in 0..<key_state.len:
  key_state[i] = grey

proc keyboard(key_state: seq[guess_t], alphabet: string) =
  const num_lines = 5
  var frontspace: string
  echo "--- Letters ---"

  for i in 0 ..< key_state.len:
    if alphabet[i] in "qaz": stdout.write(frontspace) 

    stdout.setForeGroundColor(key_state[i].colour)
    stdout.write(alphabet[i]) 
    stdout.write(" ") 
    stdout.reset_attributes()

    if alphabet[i] in "plm":
      echo ""
      frontspace.add(" ")

  echo "---------------"
  for i in 0..<num_lines:
    eraseLine()
    cursorUp 1
  eraseLine()

echo r"\ \        / / | |                          | |         "
echo r" \ \  /\  / /__| | ___ ___  _ __ ___   ___  | |_ ___    "
echo r"  \ \/  \/ / _ \ |/ __/ _ \| '_ ` _ \ / _ \ | __/ _ \   "
echo r"   \  /\  /  __/ | (_| (_) | | | | | |  __/ | || (_) |  "
echo r" _  \/ _\/ \___|_|\___\___/|_| |_| |_|\___|  \__\___/ _ "
echo r"| \ | (_)           \ \        / /          | | |    | |"
echo r"|  \| |_ _ __ ___    \ \  /\  / /__  _ __ __| | | ___| |"
echo r"| . ` | | '_ ` _ \    \ \/  \/ / _ \| '__/ _` | |/ _ \ |"
echo r"| |\  | | | | | | |    \  /\  / (_) | | | (_| | |  __/_|"
echo r"|_| \_|_|_| |_| |_|     \/  \/ \___/|_|  \__,_|_|\___(_)"

var guessed_chars: string
for i in 0 ..< 6:
  var guess: string

  while(guess.len != 5 or guess notin word_list):
    keyboard(key_state, alphabet)
    let ok = readlinefromstdin("Enter your guess: ", guess)
    cursorUp 1
    eraseLine()
    if not ok: quit(0)
    if(guess == "--cheat"):
      echo " The word is ", word

  guessed_chars.add(guess)
  var valid = checkguess(guess, word)

  for i in 0 ..< valid.len:
    stdout.setForeGroundColor(valid[i].colour)
    stdout.write(guess[i]) 

    # for the keboard printing colours
    var idx = alphabet.find(guess[i])

    # if an earlier letter/guess sets a letter don't overwrite it 
    if key_state[idx] == green: continue
    # if yellow check if there is a green one and update else skip
    if key_state[idx] == yellow: 
      if valid[i] == green: 
        key_state[idx] = green
      continue

    key_state[idx] = valid[i]
    if key_state[idx] == grey: # hide it 
      key_state[idx] = black

  stdout.reset_attributes()
  echo ""

  if guess == word:
    echo "Congratz you got the word!"
    break
