{.passC: "-std=c++20 -I/usr/include/eigen3 -fopenmp" .}
{.passL: "-lqgot_pub -fopenmp".}

# using c libraries 
proc printf(format: cstring): cint {.importc, varargs, header: "stdio.h",
  discardable.}


# using header only c++ library 
type 
  olitimer* {.header: "<olistd/timer>", importcpp: "olistd::Timer".} = object

proc stop(timer: olitimer) {.importcpp: "#.stop()".}

#using a shared c++ lib

proc qgotinit*() {.importcpp: "qgot_pubinit", dynlib: "qgot_pub.so".}

const state_hpp = "<qgot_public/math-code/state.hpp>"

type qgotstate {.header: state_hpp, importcpp: "QGot::State".} = 
  object

proc newstate(size: int) :qgotstate {.importcpp: "QGot::State{#}", header: state_hpp, constructor.}
proc print(state: qgotstate) {.importcpp: "#.print()".}
proc random(state: qgotstate) {.importcpp: "#.set_random_state()".}

proc main() = 

  var timer: olitimer

  echo "hi"

  const
    x = 1
    y = 2

  const 
    sum = x+y

  echo "x + y = ", sum

  printf("printf x + y\n")

  timer.stop()

  # var state: qgotstate
  var state = newstate(4)
  state.random()
  state.print()


main()
