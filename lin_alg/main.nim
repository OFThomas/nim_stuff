import arraymancer
import sequtils
import neo

import complex

proc arrayman() = 

  # var v: array[2, float64] = [1, 0] 
  var v = [[2, 0], [0,2]].totensor().astype(Complex[float64])
  echo v
  var l = [2.0, 1.0].totensor().astype(Complex[float64])
  echo l

  echo v * l
  let t = ([1.0, 0.0]).totensor()


  echo t

  var ident = newTensor[Complex[float64]](2)

  echo ident


proc neom() = 

  let m1 = randommatrix(2,2)
  echo m1

  let ident = matrix(@[
  @[Complex64 1.0, 0.0],
  @[Complex64 0.0, 1.0]])
  echo ident

  let v = vector([Complex64 1, 1])
  echo v

  echo ident * v

arrayman()
neom()
