import sequtils
import random
import complex

# based on the Neo library 
# https://github.com/andreaferretti/neo/blob/master/neo/dense.nim

type 
  Matrix*[T] = object
    rows*, cols*: int
    data*: seq[T]

  Vector*[T] = object
    len*: int
    data*: seq[T]

proc vector*[T](data: seq[T]): Vector[T] = 
  result = Vector[T](len: data.len, data: data)

proc vector*[T](size: int): Vector[T] =
  result = Vector[T](len: size, data: sequtils.repeat(0.0, size))

proc matrix*[T](rows, cols: int): Matrix[T] = 
  result = Matrix[T](rows: rows, cols: cols, data: sequtils.repeat(0.0, rows * cols))

# sq matrix 
proc matrix*[T](n: int, data: seq[T]): Matrix[T] =
  result = Matrix[T](rows: n, cols: n, data: data)

proc matrix*[T](rows, cols: int, data: seq[T]): Matrix[T] =
  result = Matrix[T](rows: rows, cols: cols, data: data)

# complex valued
proc matrixc*[T](rows, cols: int, data: seq[T]): Matrix[Complex[T]] = 
  result = Matrix[Complex[T]](rows: rows, cols: cols, data: data.mapit(complex(it)))

proc matrixc*[T](n: int, data: seq[T]): Matrix[Complex[T]] = 
  result = Matrix[Complex[T]](rows: n, cols: n, data: data.mapit(complex(it)))

proc makematrix*[T](rows, cols: int, f: proc(r, c: int): T): Matrix[T] =
  result = matrix[T](rows, cols, newSeq[T](rows * cols))
  for r in 0 ..< rows:
    for c in 0 ..< cols:
      result.data[r * cols + c] = f(r, c)

template makematrixtemplate*(T: typedesc, rows, cols: int, f: untyped): auto = 
  var res = matrix[T](rows, cols, newSeq[T](rows*cols))
  for r {.inject.} in 0 ..< rows:
    for c {.inject.} in 0 ..< cols:
      res.data[r * cols + c] = f
  res

proc identity*[T](n: int) : Matrix[T] = 
  result = makematrixtemplate(T, n, n, if r == c: cast[T](1.0) else: cast[T](0.0))

proc constantmatrix*[T](rows, cols: int, val: T): Matrix[T] = 
  matrix[T](rows, cols, sequtils.repeat(val, rows * cols))

proc randommatrix*[T](rows, cols: int, max: float = 1.0): Matrix[T] =
  result = matrix[T](rows, cols, newSeq[T](rows * cols))
  for i in 0 ..< rows * cols:
    result.data[i] = cast[T](rand(max))

proc randommatrix*[T](n: int, max: float = 1.0): Matrix[T] =
  randommatrix[T](n, n, max)

# Accessing 

proc `[]`*[T](m: Matrix[T], r, c: int): T {.inline.} =
  result = m.data[r * m.cols + c] 

proc `[]`*[T](m: var Matrix[T], r, c: int): var T {.inline.} =
  result = m.data[r * m.cols + c] 

proc `[]=`*[T](m: var Matrix[T], r,c: int, val: T) {.inline.} =
  m.data[r * m.cols + c] = val

proc `[]`*[T](v: Vector[T], idx: int): T {.inline.} = 
  result = v.data[idx]

proc `[]`*[T](v: var Vector[T], idx: int): var T {.inline.} = 
  result = v.data[idx]

proc `[]=`*[T](v: var Vector[T], idx: int, val: T) {.inline.} =
  v.data[idx] = val


proc row*[T](mat: Matrix[T], r: int) : Vector[T] =
  result = Vector[T](len: mat.cols)
  for c in 0 ..< mat.cols:
    result.data.add mat[r, c]

proc col*[T](mat: Matrix[T], c: int) : Vector[T] = 
  result = Vector[T](len: mat.rows)
  for r in 0 ..< mat.rows:
    result.data.add mat[r, c]

proc clone*[T](v: Vector[T]): Vector[T] =
  var dcopy = v.data
  return vector(dcopy)

proc clone*[T](m: Matrix[T]): Matrix[T] =
  var dcopy = m.data
  return matrix(m.rows, m.cols, dcopy)

# Arithmitic

# ======== + 
proc `+=`*[T](lhs: var Matrix[T], rhs: Matrix[T]) =
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.cols:
      lhs[r, c] += rhs[r, c]

proc `+`*[T](lhs, rhs: Matrix[T]): Matrix[T] =
  result = lhs
  result += rhs

proc `+=`*[T](lhs: var Vector[T], rhs: Vector[T]) =
  for idx in 0 ..< lhs.len:
    lhs[idx] += rhs[idx]

proc `+`*[T](lhs: Vector[T], rhs: Vector[T]): Vector[T] =
  result = lhs
  result += rhs

# ======== -
proc `-=`*[T](lhs: var Matrix[T], rhs: Matrix[T]) =
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.cols:
      lhs[r, c] -= rhs[r, c]

proc `-`*[T](lhs, rhs: Matrix[T]): Matrix[T] =
  result = lhs
  result -= rhs

proc `-=`*[T](lhs: var Vector[T], rhs: Vector[T]) =
  for idx in 0 ..< lhs.len:
    lhs[idx] -= rhs[idx]

proc `-`*[T](lhs: Vector[T], rhs: Vector[T]): Vector[T] =
  result = lhs
  result -= rhs

# ======== *
proc `*=`*[T](lhs: var Matrix[T], rhs: Matrix[T]) = 
  var lhscopy = lhs
  for r in 0 ..< lhs.rows:
    # var row_copy = lhs.row(r)
    for c in 0 ..< lhs.cols:
      lhs[r, c] = 0.0
      for k in 0 ..< lhs.cols:
        lhs[r, c] += lhscopy[r, k] * rhs[k, c]

proc `*`*[T](lhs, rhs: Matrix[T]): Matrix[T] = 
  result = lhs
  result *= rhs

# matrix vector product
proc `*=`*[T](lhs: Matrix[T], rhs: var Vector[T]) = 
  var vcopy = rhs
  for r in 0 ..< lhs.rows:
    rhs[r] = 0.0
    for c in 0 ..< lhs.cols:
      rhs[r] += lhs[r, c] * vcopy[c]

proc `*`*[T](lhs: Matrix[T], rhs: Vector[T]): Vector[T] = 
  result = rhs
  lhs *= result

# vector matrix product
proc `*=`*[T](lhs: var Vector[T], rhs: Matrix[T]) = 
  var vcopy = lhs
  for c in 0 ..< lhs.len:
    lhs[c] = 0.0
    for r in 0 ..< rhs.rows:
      lhs[c] += vcopy[r] * rhs[r,c]

proc `*`*[T](lhs: Vector[T], rhs: Matrix[T]): Vector[T] = 
  result = lhs
  result *= rhs

# vector vector product 
proc `*`*[T](lhs: Vector[T], rhs: Vector[T]): T = 
  result = 0.0
  for idx in 0 ..< lhs.len:
    result += lhs[idx] * rhs[idx]


# scalars matrices 
proc `*=`*[T, ScalarT: SomeNumber](lhs: var Matrix[T], rhs: ScalarT) = 
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.cols:
      lhs[r,c] *= T(rhs)

proc `*`*[T, ScalarT: SomeNumber](lhs: Matrix[T], rhs: ScalarT): Matrix[T] = 
  result = lhs
  result *= rhs

proc `*=`*[ScalarT: SomeNumber, T](lhs: ScalarT, rhs: var Matrix[T]) = 
  for r in 0 ..< rhs.rows:
    for c in 0 ..< rhs.cols:
      rhs[r,c] *= T(lhs)

proc `*`*[ScalarT: SomeNumber, T](lhs: ScalarT, rhs: Matrix[T]): Matrix[T] = 
  result = rhs
  result *= lhs

# scalar vectors 
proc `*=`*[T, ScalarT: SomeNumber](lhs: var Vector[T], rhs: ScalarT) = 
  for i in 0 ..< lhs.len:
      lhs[i] *= T(rhs)

proc `*`*[T, ScalarT: SomeNumber](lhs: Vector[T], rhs: ScalarT): Vector[T] = 
  result = lhs
  result *= rhs

proc `*=`*[ScalarT: SomeNumber, T](lhs: ScalarT, rhs: var Vector[T]) = 
  for r in 0 ..< rhs.len:
      rhs[r] *= T(lhs)

proc `*`*[ScalarT: SomeNumber, T](lhs: ScalarT, rhs: Vector[T]): Vector[T] = 
  result = rhs
  result *= lhs


# ========= /
#
# scalar matrices
proc `/=`*[T, ScalarT: SomeNumber](lhs: var Matrix[T], rhs: ScalarT) = 
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.cols:
      lhs[r,c] /= T(rhs)

proc `/`*[T, ScalarT: SomeNumber](lhs: Matrix[T], rhs: ScalarT): Matrix[T] = 
  result = lhs
  result /= rhs

# scalar vectors
proc `/=`*[T, ScalarT: SomeNumber](lhs: var Vector[T], rhs: ScalarT) = 
  for r in 0 ..< lhs.len:
      lhs[r] /= T(rhs)

proc `/`*[T, ScalarT: SomeNumber](lhs: Vector[T], rhs: ScalarT): Vector[T] = 
  result = lhs
  result /= rhs

# random stuff that is a hack atm
proc to_complex*[T](m: Matrix[T]): Matrix[Complex[T]] = 
  result = matrix[Complex[T]](m.rows, m.cols, m.data.mapit(complex(it)))
  # m.data.applyit(complex(it))

# Pretty printing
proc `$`*[T](v: Vector[T]): string =
  result = "[ "
  for i in 0 ..< v.len - 1:
    result &= $(v[i]) & " ]\n"
  result &= "[ " & $(v[v.len - 1]) & " ]"

proc toStringHorizontal[T](v: Vector[T]): string =
  result = "[ "
  for i in 0 ..< (v.len - 1):
    result &= $(v[i]) & "\t"
  result &= $(v[v.len - 1]) & " ]"

proc `$`*[T](m: Matrix[T]): string =
  result = ""
  for i in 0 ..< m.rows - 1:
    result &= toStringHorizontal(m.row(i)) & "\n"
  result &= toStringHorizontal(m.row(m.rows - 1)) & ""


proc main() =

  const dim: int = 2
  let m = matrix(dim, dim, @[
  0.0, 1.0, 
  1.0, 0.0].mapit(complex(it)))
  echo m,  "\n"

  let m1 = matrix(dim, @[1.0, 0.0, 0.0, 1.0].mapit(complex(it)))
  echo m1, "\n"

  var m2 = m + m1
  echo m2, "\n"

  m2 += m2
  echo m2, "\n"

  var ident = identity[Complex64](dim)
  echo ident, "\n"

  ident += ident
  echo ident, "\n"

  var mat = randommatrix[float](dim)
  echo mat, "\n"

  let cmat = mat.to_complex
  echo cmat, "\n"

  let mc = matrixc(dim, dim,  @[1.0, 0.0, 0.0, 1.0])
  echo mc, "\n"
  
  let randmc = randommatrix[Complex64](dim, dim)
  echo randmc, "\n"

  echo (3.0 * identity[float](dim) * 2) / 16.0


  var v = vector[float](dim)
  v[0] = 1.0
  echo v, "\n"

  echo 2 * v + v / 0.5

  var identr = 2 * identity[float](dim)
  echo identr * v

  echo " v ", v
  v *= identr
  echo v

  var m23 = matrix(dim, @[0.0, 1.0, 0.0, 0.0])
  var m32 = matrix(dim, @[1.0, 0.0, 0.0, 0.0])

  echo m23 * m32
  echo m32 * m23

  var copy_m23 = m23
  var copy_m32 = m32
  copy_m23 *= copy_m32
  echo copy_m23

  copy_m32 *= m23
  echo copy_m32

  echo v * identr/2

  echo v * identr * v




when isMainModule:
  main()
