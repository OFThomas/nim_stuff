import arraymancer, strformat
import csvtools

discard """
A fully-connected ReLU network with one hidden layer, trained to predict y from x
by minimizing squared Euclidean distance.
"""

type Dataset = object
  x, y: float

const dssize = 10

proc gendataset(size: Natural) : seq[Dataset] =
  var dataset: seq[Dataset]
  for i in 0 ..< size:
    var ds: Dataset
    ds.x = float(i)
    ds.y = 2.0 * float(i)
    dataset.add(ds)
  return dataset

let ds = gendataset(dssize)
echo ds

const csv_path = "dataset.csv"
ds.writetocsv(csv_path)

let mytensor = read_csv[float32](csv_path)
echo "Tensor ", mytensor

# quit(0)
# Environment variables

# batch_size is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
const 
  batch_size: int = dssize
  d_in: int = 1
  d_hidden: int = 1
  d_out: int = 1

# Create the autograd context that will hold the computational graph
let ctx = newContext Tensor[float32]

# Create random Tensors to hold inputs and outputs, and wrap them in Variables.
var xtrain = randomTensor[float32](batch_size, d_in, 1'f32)

var datasett = read_csv[float32](csv_path)

# take the first column 
xtrain = datasett[_, 0]

echo "x ", xtrain
let
  x = ctx.variable(xtrain)
  # the last column
  y = datasett[_, ^1]

echo "y ", y

# Define the model.

network TwoLayersNet:
  layers:
    fc1: Linear(d_in, d_hidden)
    # fc2: Linear(d_hidden, d_out)
  forward x:
    x.fc1
    # x.fc1.relu.fc2

let
  model = ctx.init(TwoLayersNet)
  optim = model.optimizer(SGD, learning_rate = 1e-4'f32)

# Training
const num_epochs = 1_000_0 

for t in 0 ..< num_epochs:
  let
    y_pred = model.forward(x)
    loss = y_pred.mse_loss(y)

  if t == num_epochs - 1:
    echo "x ", x.value
    echo "y pred ", y_pred.value
    echo &"Epoch {t}: loss {loss.value[0]}"

  loss.backprop()
  optim.update()

echo model.fc1.weight.value
echo model.fc1.bias.value

# echo model.fc2.weight.value
# echo model.fc2.bias.value
