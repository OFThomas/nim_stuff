
### Nim install (unix)
```
curl https://nim-lang.org/choosenim/init.sh -sSf | sh
```

Add to path add your `~/.bashrc` (i.e. paste the following line at the bottom 
of the file)
```
export PATH=~/.nimble/bin:$PATH
```

### To use nim

To compile to c (using c compiler):
```
nim c -r test.nim
```

To compile to c++ (using c++ compiler):
```
nim cpp -r test.nim
```

## Nim setup for vim 

```
https://github.com/BitR/ycm-nimsuggest
```
