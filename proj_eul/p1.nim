import std/strutils
import std/os

proc fizzbuzz(last: Natural) : Natural =
  var sum: Natural = 0
  for i in 0 ..< last:
    # echo "i / 3 ", i mod 3
    if i mod 3 == 0 : sum += i
    elif i mod 5 == 0: sum += i
  return sum

if paramCount() > 0:
  let last: Natural = parseInt(paramStr(1))
  echo "last ", last

  echo fizzbuzz(last)
